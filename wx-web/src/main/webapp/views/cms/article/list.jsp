<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/12/13
  Time: 下午9:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <%--<link rel="stylesheet" href="//res.layui.com/layui/dist/css/layui.css"  media="all">--%>
    <link rel="stylesheet" href="${basePath}/static/plugins/layui/css/layui.css"  media="all">
    <link rel="stylesheet" href="${basePath}/static/css/ztree/metro/ztree.css"  media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
    <style>
        .mainLeft{
            float: left;
            width: 15%;
        }
        .mainRight{
            float: right;
            width: 85%;
        }
    </style>
</head>
<body>

<%--查询条件以及操作按钮--%>
<br>
<div class="layui-form">
    <%--查询条件--%>
    <div class="layui-form-item layui-form-pane">
        <label class="layui-form-label">文章标题</label>
        <div class="layui-input-inline">
            <input type="text" name="article_name" required  lay-verify="required" placeholder="文章标题" autocomplete="off" class="layui-input" id="article_name">
        </div>
        <%--操作按钮--%>
        <button class="layui-btn"  id="search">
            <i class="layui-icon">&#xe615;</i>搜索
        </button>
        <shiro:hasPermission name="article:add">
            <button class="layui-btn" id="add">
                <i class="layui-icon">&#xe608;</i>添加
            </button>
        </shiro:hasPermission>

    </div>
</div>

<%--树形菜单--%>
<div class="mainLeft">
    <input id="channel_pk" name="channel_pk"  autocomplete="off"  class="layui-input" type="hidden"/>
    <ul id="ztree" class="ztree" style="margin-top:0; width:160px;"></ul>
</div>


<%--分页数据列表--%>
<div class="mainRight">
    <div class="layui-form">
        <table id="tablelist" class="layui-table" lay-data="{height: 'full-80', cellMinWidth: 20,page: true, limit:30, url:'${basePath}/article/pages'}" lay-filter="table">
            <thead>
            <tr>
                <%--<th lay-data="{type:'checkbox'}">ID</th>--%>
                <%--<th lay-data="{field:'article_state', width:80, sort: true}">状态</th>--%>
                <th lay-data="{field:'article_pk', width:50,sort: true}">ID</th>
                <th lay-data="{field:'article_title',width:400}">标题</th>
                <th lay-data="{field:'article_sendtime', sort: true, align: 'center'}">发文时间</th>
                <th lay-data="{field:'article_author', sort: true, align: 'center'}">创建人</th>
                <th lay-data="{fixed: 'right', align:'center', toolbar: '#operation'}"></th>
            </tr>
            </thead>
        </table>
    </div>
</div>

<script type="text/html" id="operation">
    <shiro:hasPermission name="article:edit">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    </shiro:hasPermission>
    <shiro:hasPermission name="article:delete">
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </shiro:hasPermission>
</script>


<script src="${basePath}/static/plugins/layui/layui.js"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.config({
        base: '${basePath}/static/js/'
    }).use(['table','ztree'], function(){
        var $ = layui.jquery;
        var table = layui.table;

        //监听表格复选框选择
        table.on('checkbox(table)', function(obj){
            console.log(obj)
        });

        //监听工具条
        table.on('tool(table)', function(obj){
            var data = obj.data;
            if(obj.event === 'del'){
                article_delete(data.article_pk);
            } else if(obj.event === 'edit'){
//                layer.alert('编辑行：<br>'+ JSON.stringify(data))
                edit(data.article_pk);
            }
        });


        $('#add').on('click', function() {
            var index = layer.open({
                title: '添加',
                maxmin: true,
                type: 2,
                content: 'edit.jsp',
                area : ['950px','520px'],
                success: function(layero, index) {

                },
                end : function () {
                    table.reload("tablelist");
                }
            });
            layer.full(index);
        });


        //过滤查询
        $("#search").on('click',function () {
            //执行重载
            table.reload('tablelist', {
                url: '${basePath}/article/pages',

                where: {
//                    key: {
                        article_name:$("#article_name").val(),
//                    }
                  },
                  page: {
                      curr: 1 //重新从第 1 页开始
                  }

            });
          
            
        });


        //编辑操作
        edit = function (article_pk){
            var index=layer.open({
                title : '编辑',
                maxmin : true,
                type : 2,
                content : '${basePath}/article/list?article_pk='+article_pk,
                area : ['950px','520px'],
                success : function(layero,index){
                    var body = layer.getChildFrame('body', index); //巧妙的地方在这里哦
                    body.contents().find("#article_pk").val(article_pk);
                },
                end : function () {
                    table.reload("tablelist");
                }
            });
            layer.full(index);
        };


        //删除记录
        article_delete = function (data) {
            var flag = false;
            layer.confirm("确认删除此数据吗？", {icon: 3, title: '提示'},
                function (index) {      //确认后执行的操作
                    $.ajax({
                        type: 'post',
                        url: '${basePath}/article/delete',
                        data: {article_pk: data},
                        success: function (response) {
                            if (response.state == "success") {
                                layer.close(index);
                                parent.layer.msg("删除成功");
                                table.reload("tablelist");
                            } else if (response.state == "fail") {
                                parent.layer.alert(response.msg);
                            }
                        },
                        error: function (response) {
                            layer.close(index);
                            parent.layer.alert(response.msg);
                        }
                    });
                },
                function (index) {      //取消后执行的操作
                    flag = false;
                });
        };

        var setting = {
            view: {
//                addHoverDom: addHoverDom,
//                removeHoverDom: removeHoverDom,
                selectedMulti: false
            },
            check: {
                enable: false
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            edit: {
                enable: false
            },
            callback: {
//                onClick: function(e, treeId, treeNode) {
//                    console.log(treeNode);
//                }
                onClick:function(event, treeId, treeNode){
                    onCheck(treeNode);
                }
            }
        };

        function onCheck(treeNode){
            //树形菜单点击左键显示对应的文章
            table.reload('tablelist', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                ,
                url: '${basePath}/article/pages'
                ,where: {channel_pk:treeNode.id} //设定异步数据接口的额外参数
                //,height: 300
            });
        }

        $(document).ready(function() {
            var list_url = "${basePath}/channel/article_ztree_list";
            var roles_pk=$("#roles_pk").val();
            $.ajax({
                type:'post',
                url:list_url,
                data:{roles_pk:roles_pk},
                success:function (response) {
//                    inittree(response)
                    $.fn.zTree.init($("#ztree"), setting, response);
                }
            })

        });

    });
</script>


</body>
</html>
